﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace basicgame
{
    class main
    {

        static string playername;
        static int playerstrength = 5;
        static int playerhealth = 5;
        static int playerdefense = 5;
        static int playerspeed = 5;
        static int playerluck = 5;
        static int playerspecial = 5;


        static int monsterattack = 3;
        static int monsterhealth = 30;
        static string monstername;

        static string location;
        static string command;
        static string consoletitle = "Hunter's quest";


        static void Main(string[] args)
        {
            Console.Title = consoletitle;
            CreatePlayer();
            ProcessCombat();
            StartNextCombat();

        }

        static void ProcessCombat() 
        {
            int monstadamage;
            int playadamage;
            int playerhealthbar = (playerhealth * 10);
            string monstername = "cuzao";
            while (playerhealth > 0 && monsterhealth > 0)
            {
                string command;
                int monsterattackog = monsterattack;
                Console.ReadLine();
                Console.Clear();
                Console.WriteLine("Your health is: " + playerhealthbar);
                Console.WriteLine(monstername + " health is: " + monsterhealth);
                Console.WriteLine("What will you do?");
                command = Console.ReadLine();
                if (command == "attack")
                {
                    Console.WriteLine("You attack " + monstername + " for " + playerstrength + " damage!");
                    monsterhealth -= playerstrength;
                    if((monsterattack - playerdefense) > 0)
                    {
                        monstadamage = (monsterattack - playerdefense);
                    }else
                    {
                        monstadamage = 0;
                    }

                    Console.WriteLine(monstername + " attacked you for " + monstadamage + " damage!");
                    playerhealthbar -= monstadamage;
                }
                else if (command == "defend")
                {
                    monsterattack = monsterattack / 2;
                    Console.WriteLine("You defend!");
                    if ((monsterattack - playerdefense) > 0)
                    {
                        monstadamage = (monsterattack - playerdefense);
                    }
                    else
                    {
                        monstadamage = 0;
                    }
                    Console.WriteLine(monstername + " attacked you for " + monstadamage + " damage!");
                    playerhealthbar -= monstadamage;
                    monsterattack = monsterattackog;
                }
                else if (command == "die")
                {
                    monsterattack = monsterattack * 5;
                    Console.WriteLine("You die!");
                    Console.WriteLine(monstername + " attacked you for " + monsterattack + " damage!");
                    playerhealthbar -= monsterattack;
                }
                else
                {
                    Console.WriteLine("Invalid Command!");
                }
            }
            if (monsterhealth <= 0)
            {
                Console.WriteLine("You have killed the " + monstername + "! The people of " + location + " declared " + playername + " a hero!");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("You died a valiant hunter.");
                Console.ReadLine();
            }
        }

        static void CreatePlayer()
        {
            int []player = new int[6];
            string[] statname = new string[6];
            player[0] = 5;
            player[1] = 5;
            player[2] = 5;
            player[3] = 5;
            player[4] = 5;
            player[5] = 5;
            statname[0] = "Health";
            statname[1] = "Strength";
            statname[2] = "Defense";
            statname[3] = "Speed";
            statname[4] = "Luck";
            statname[5] = "Special";
            int points = 10;
            int pointsog;
            int additive = 0;
            Console.WriteLine("Time to create your character! What is your name?");
            playername = Console.ReadLine();
            Console.WriteLine("There are six stats to build from:");
            Console.WriteLine("Health determines how much damage you can take!");
            Console.WriteLine("Strength determines the damage you deal!");
            Console.WriteLine("Defense negates damage taken!");
            Console.WriteLine("Speed determines how fast you can act in combat!");
            Console.WriteLine("Luck determines how lucky you are in battle!");
            Console.WriteLine("Special determines how fast you build your special move!");
            Console.ReadLine();
            Console.WriteLine("Spend points to raise your stats");
            pointsog = points;
            for(int x = 0; x < 6; x++)
            {
                Console.WriteLine();
                Console.WriteLine("Points left: " + points);
                if(points >= 0)
                {
                    Console.WriteLine("Current " + statname[x] + " is " + player[x]);
                    Console.WriteLine("Spend how many points?");
                    try
                    {
                        additive = int.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Invalid Number");
                        x--;
                        continue;
                    }
                    if((points - additive) >= 0)
                    {
                        player[x] = (player[x] += additive);
                        Console.WriteLine("New " + statname[x] + " is " +player[x]);
                        points -= additive ;
                        pointsog -= additive;
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Points Exceeded! Try again");
                        x--;
                        points = pointsog;
                        continue;
                    }

                }
                else
                {
                    Console.WriteLine("No points left for " +statname[x] +"!");
                }
                
            }
            if (points > 0)
            {
                Console.WriteLine("There are still points to spend! Do you wish to spend them on any ability?");
                if (Console.ReadLine() == "yes" || Console.ReadLine() == "Yes")
                {
                    Console.WriteLine("Type the ability:");
                    Console.WriteLine("[1] Health [2] Strength [3] Defense");
                    Console.WriteLine("[4] Speed [5] Luck [6] Special");
                    switch (Console.ReadLine())
                        {
                        case "1": player[0] += points;
                            points = 0;
                            break;

                        case "2": player[1] += points;
                            points = 0;
                            break;

                        case "3": player[2] += points;
                            points = 0;
                            break;

                        case "4": player[3] += points;
                            points = 0;
                            break;

                        case "5": player[4] += points;
                            points = 0;
                            break;

                        case "6": player[5] += points;
                            points = 0;
                            break;

                        default: Console.WriteLine("Invalid ability, try again");
                            break;

                        }
                } else
                {
                    Console.WriteLine("You are done creating your character!");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("You are done creating your character!");
                Console.ReadLine();
            }
            playerhealth = player[0];
            playerstrength = player[1];
            playerdefense = player[2];
            playerspeed = player[3];
            playerluck = player[4];
            playerspecial = player[5];
            Console.Clear();
            Console.WriteLine("Stats are as follow:");
            Console.WriteLine(playername);
            Console.WriteLine();
            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine( statname[i] + ":" + player[i]);
            }
            Console.ReadLine();
            
        }

        static void LoadMonster()
        {
        }

        static void StartNextCombat()
        {
            LoadMonster();
        }


    }
}
