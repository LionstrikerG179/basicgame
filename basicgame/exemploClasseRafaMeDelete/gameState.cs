﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exemploClasseRafaMeDelete
{
    public enum GameState
    {
        abertura = 1,
        lobby = 2,
        criaMonstro = 3,
        listaMonstros = 4,
        statusMonstro = 5,
        saindo = 6
    }
}
