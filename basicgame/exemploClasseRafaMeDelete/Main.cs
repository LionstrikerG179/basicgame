﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exemploClasseRafaMeDelete
{
    public class main
    {
        public static Engine engine;
        public static GameState gameState = GameState.abertura;

        static void Main(string[] args)
        {
            engine = new Engine();
            while (gameState != GameState.saindo)
            {
                gameState = engine.AtualizarPagina(gameState);
            }
        }   
    }
}
