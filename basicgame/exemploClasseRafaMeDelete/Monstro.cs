﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exemploClasseRafaMeDelete
{
    public class Monstro
    {
        public string name;
        public int strength;
        public int health;
        public int defense;
        public int speed;
        public int luck;
        public int special;
        public int bonus;

        public Monstro()
        {
            var possibleNames = new List<String> { "cuzao", "bastardo", "tolo", "palhaço", "escroto", "William Wallace" };

            var random = new Random();
            this.name = possibleNames[Convert.ToInt32(random.NextDouble() * 5)];

            if(this.name == "William Wallace"){
                this.bonus = 10;
            }else {
                this.bonus = 0;
            }

            this.strength = Convert.ToInt32(random.NextDouble() * 6) + 1 + this.bonus;
            this.health = Convert.ToInt32(random.NextDouble() * 6) + 1 + this.bonus;
            this.defense = Convert.ToInt32(random.NextDouble() * 6) + 1 + this.bonus;
            this.speed = Convert.ToInt32(random.NextDouble() * 6) + 1 + this.bonus;
            this.luck = Convert.ToInt32(random.NextDouble() * 6) + 1 + this.bonus;
            this.special = Convert.ToInt32(random.NextDouble() * 6) + 1 + this.bonus;
            this.bonus = Convert.ToInt32(random.NextDouble() * 6) + 1 + this.bonus;
        }

        public void ShowStats()
        {
            Console.WriteLine(string.Format("Monster : {0} {1}",this.name, Environment.NewLine));
            Console.WriteLine(string.Format("Health: {0}", this.health));
            Console.WriteLine(string.Format("Strenght: {0}", this.strength));
            Console.WriteLine(string.Format("Defense: {0}", this.defense));
            Console.WriteLine(string.Format("Speed: {0}", this.speed));
            Console.WriteLine(string.Format("Luck: {0}", this.luck));
            Console.WriteLine(string.Format("Special: {0}", this.special));
        }
    }
}
