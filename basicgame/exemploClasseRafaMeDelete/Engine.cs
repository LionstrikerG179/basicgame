﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exemploClasseRafaMeDelete
{
    public class Engine
    {
        public List<Monstro> monstros;
        public Monstro monstro;
        public string action;

        public Engine()
        {
            monstros = new List<Monstro>();
        }

        public GameState AtualizarPagina(GameState gameState)
        {
            Console.Clear();
            switch (gameState)
            {
                case GameState.abertura:
                    Console.WriteLine("Welcome to enemyGenerator");
                    Console.WriteLine("Press any key to continue");
                    Console.Read();       
                    return GameState.lobby;
                case GameState.lobby:
                    Console.WriteLine("Choose your action");
                    Console.WriteLine("'c' - Create Monster");
                    if (monstros.Count() > 0)
                    {
                        Console.WriteLine("'l' - Monster List");
                    }
                    Console.WriteLine("'q' - exit");
                    action = Console.ReadLine();
                    switch (action)
                    {
                        case "c":        
                            return GameState.criaMonstro;
                        case "l":
                            return GameState.listaMonstros;
                        case "q":
                            return GameState.saindo;
                        default:
                            return GameState.lobby;
                    }
                case GameState.listaMonstros:
                    Console.WriteLine(string.Format("There are {0} Monsters, wich do you want to see: {1}", monstros.Count, Environment.NewLine));
                    var i = 0;
                    var max = monstros.Count;
                    for (i = 0, max = monstros.Count; i < max; i++)
                    {
                        Console.WriteLine("'{0}' - {1}", i, monstros[i].name);
                    }
                    Console.WriteLine(string.Format("{0}'l' - Return to lobby", Environment.NewLine));
                    action = Console.ReadLine();
                    var actionInt = 0;
                    if (action == "l")
                    {
                        return GameState.lobby;
                    }
                    else if (Int32.TryParse(action, out actionInt) && actionInt < monstros.Count)
                    {
                        monstro = monstros[actionInt];
                        return GameState.statusMonstro;
                    }
                    else
                    {
                        return GameState.listaMonstros;
                    }
                case GameState.criaMonstro:
                    monstro = new Monstro();
                    monstros.Add(monstro);
                    Console.WriteLine("A monster was created, what will you do");
                    Console.WriteLine("'l' - Return to lobby");
                    Console.WriteLine("'m' - See Monster Stats");
                    action = Console.ReadLine();
                    switch(action)
                    {
                        case "l":
                            return GameState.lobby;
                        case "m":
                            return GameState.statusMonstro;
                        default:
                            return GameState.criaMonstro;

                    }
                case GameState.statusMonstro:
                    monstro.ShowStats();
                    Console.WriteLine(string.Format("{0}'c' - Create another monster", Environment.NewLine));
                    Console.WriteLine(string.Format("{0}'l' - Return to lobby", Environment.NewLine));
                    action = Console.ReadLine();
                    switch (action)
                    {
                        case "c":        
                            return GameState.criaMonstro;
                        case "l":
                            return GameState.lobby;
                        default:
                            return GameState.statusMonstro;
                    }
                default:
                    return GameState.lobby;
            }
        }
    }
}
 
